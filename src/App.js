import React,{Component} from "react";
//import logo from './logo.svg';
import './components/css/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { NavigationBar } from './components/js/NavigationBar';
import ProductList from './components/js/ProductList';
import Details from './components/js/details';
import Cart from './components/js/cart';
import PNF from './components/js/404';
import Myaccount from './components/js/myaccount'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';


class App extends Component{
  render(){
    return(
      <React.Fragment>
        <Router>
          <NavigationBar />
          <Switch>
              <Route exact path="/" component={ProductList} />
              <Route path="/myaccount" component={Myaccount} />
              <Route path="/cart" component={Cart}/>
              <Route component={PNF} />
          </Switch>
        </Router>
      </React.Fragment>
    )
  }
}
export default App;
