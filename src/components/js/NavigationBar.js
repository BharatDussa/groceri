import React from 'react';
import { Nav, Navbar, Form, FormControl } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`

  .navbar { background-color: #222; }
  a, .navbar-nav, .navbar-light .nav-link {
    color: white;
    &:hover { color: #9FFFCB; }
  }
  .navbar-brand {
    font-size: 1.4em;
    color: white;
    &:hover { 
        
    color: #00d347; }
  }
  .form-center {
    position: absolute !important;
    left: 25%;
    right: 25%;
  }
`;
export const NavigationBar = () => (
  <Styles position="fixed">
    <Navbar expand="lg">
      <Navbar.Brand href="/">Groceri</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Form className="form-center">
        <FormControl type="text" placeholder="Search for products" className=""></FormControl>
      </Form>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Item><Nav.Link href="/myaccount"><span><i className="fas fa-user"></i></span> My Account</Nav.Link></Nav.Item> 
          <Nav.Item><Nav.Link href="/cart"><span><i className="fas fa-cart-plus"></i></span> My Cart</Nav.Link></Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </Styles>
)

