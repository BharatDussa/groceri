import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import logo from '../img/logo/logo.jpeg'
import {MyCartButton} from './parts-styles/button'
import {NavWrapper} from './parts-styles/navWrapper'


export class Navbar extends Component {
    constructor(props){
        super(props);
        this.state={
            myaccount:'My Account'
        }
    }
    render() {
        return (
            
            <NavWrapper className="navbar  navbar-expand-sm-navbar-dark px-sm-5">
            <Link to="/">
                 <img src={logo} alt="groceri" width="10%"  className="navbar-brand"></img>
            </Link>
        
          
            <input type="text" className="input" placeholder="Search..." />
            <ul className="navbar-nav align-items-center">
                   
            <li className="nav-item ml-5">
                    <Link to="/myaccount" className="nav-link">
                        <select value={this.state.myaccount}>
                            <option value="Login">My Account</option>
                            <option value="FAQ">Login</option>
                        </select>
                    </Link> 
                </li>
            
            </ul>
            
            <Link to="/cart" className="ml-auto">

                <MyCartButton>
                <span className="mr-2">
                <i className="fas fa-cart-plus"/>
                </span>
                my cart
                </MyCartButton>
                
            </Link>
            </NavWrapper>
                            
            
        )
    }
}




export default Navbar
