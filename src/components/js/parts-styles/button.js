import styled from 'styled-components'

export const MyCartButton=styled.button `
text-transform:capitalize;
font-size:1.1rem;
background:transparent;
border:0.05rem solid var(--logoGreen);
color:var(--mainWhite);
border-radius:0.5rem;
padding:0.2 rem 0.5 rem;
transition:all 0.3s ease-in-out;
&:hover{
    background:var(--mainGreen);
}
`
