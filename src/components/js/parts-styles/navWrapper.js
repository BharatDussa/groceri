import styled from 'styled-components'

export  const NavWrapper=styled.nav `
background:var(--logoGreen);

.nav-link{
    color:var(--mainWhite)!important;
    font-size:1.1rem;
    text-transform:capitalize;
    border-radius:0.5rem;
    padding:0.2rem 0.5rem;
    transition: all 0.3s ease-in-out;
    &:hover{
        background:var(--mainGreen);
    }

}

`


